const pool = require('../../connection/config/db/db_connection');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const fast2sms = require('fast-two-sms');
const UserSchema = require('../../model/user');
const saltRounds = 10;
const moment = require('moment')
var commonCon;
const mongoose = require("mongoose")
exports.userRegstration = async (rq, rs) => {
   var then = new Date();
   then = moment().format('DD MM YYYY, h:mm:ss a')
   const byEmail = await UserSchema.findOne({ email: rq.body.email });
   const byContact = await UserSchema.findOne({ contact: rq.body.contact });
   if (byEmail || byContact) {
      rs.json({
         success: false,
         message: 'Email exist, Please try another email!'
      });
   } else {
      bcrypt.hash(rq.body.password, null, null, function (err, hash) {
         const users = new UserSchema({
            userId: new mongoose.Types.ObjectId(),
            fullName: rq.body.fullName,
            email: rq.body.email,
            contact: rq.body.contact,
            created_at: then,
            updated_at: then,
            password: hash,
            status: true,
            otp: 65776363
         })
         users
            .save()
            .then(result => {
               rs.status(200).json({
                  message: "User Registered!",
                  user: result,
                  success:true
               })
            })
            .catch(err => {
               rs.status(500).json({
                  message: "Plz Enter Valid data..",
                  err: err
               })
            })
      })
   }
}


exports.userlogin = async (request, response) => {
   var email = request.body.email;
   UserSchema.findOne({ email: email }).exec().then(findData=>{
      if (findData) {
         bcrypt.compare(request.body.password, findData.password, function(err, res){
            if (res == true) {
               findData.password = undefined;
               findData.otp = undefined;
               let token = jwt.sign({ result: findData }, 'myTokenKey'
               ); 
               findData['token']=token
               response.json({
                  success: true,
                  message: 'Logged In !',
                  data: findData,
                  token:token
               })
            }else{
               response.json({
                  success: false,
                  message: 'Invalid credentials!'
               })
            }
         })
      } else {
         response.json({
            success: false,
            message: 'Invalid credentials!'
         })
      }
   });
}

exports.getAllUsers= async (req,res)=>{
 
   var size=Number(req.query.itemPerPage);
   var page =Number(req.query.pageNumber);
   if(!size){
      size=10
   }
   if(!page){
      page=1
   }
   const skip=(page-1)*size
   const users=await UserSchema.find({}).limit(size).skip(skip);
    if(users){
       res.send({
          success:true,
          page:page,
          size:size,
          data:users
       })
    }
}

exports.getUserDetails=(req,res)=>{
  UserSchema.find({userId:req.query.userId}).exec().then(user=>{
     if(user){
        user[0]['otp']=undefined
        res.send({
           success:true,
           data:user
        })
     }else{
      res.send({
         success:false,
         data:[],
      })
     }
  })
}

exports.updateUser= async (req,res)=>{
  var userId=req.query.userId
   UserSchema.updateOne({userId:userId},{$set:req.body}).exec().then(success=>{
      if(success){
         res.send({
            success:true,
            message:"Updated !"
         })
      }else{
         res.send({
            success:false,
            message:'Not Updated !'
         })
      }
   })
}

exports.deleteUser=(req,res)=>{
   const userId=req.query.userId
   UserSchema.remove({userId:userId}).exec().then(result=>{
      if(result){
         res.send({
            success:true,
            message:'Deleted !'
         })
      }else{
         res.send({
            success:false,
            message:'Not Deleted !'
         })
      }
   })
}