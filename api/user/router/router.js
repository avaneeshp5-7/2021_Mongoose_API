const exp = require('express');
const route = exp.Router();
const {checkTcken}=require('../auth/middleware/auth.middleware');
const userController=require('../controller/user')
const rout=exp.Router();
rout.post('/user_registraion',userController.userRegstration);
rout.post('/user_login',userController.userlogin);
rout.get('/all_user',checkTcken,userController.getAllUsers);
rout.get('/single_user',checkTcken,userController.getUserDetails);
rout.post('/update_user',checkTcken,userController.updateUser);
rout.post('/delete_user',checkTcken,userController.deleteUser);
module.exports=rout; 