const mongoose=require("mongoose")
const UserSchema=mongoose.Schema({
    userId:mongoose.Schema.Types.ObjectId,
    fullName:{
        type:String,
        required:true,
        unique: false
    },
    email:{
        type:String,
        required:true,
        unique: true
    },
    contact:{
        type:Number,
        required:true,
        unique: true
    },
    password:{
        type:String,
        required:true
    },
    status:{
        type:Boolean,
        required:false
    },
    created_at:{
        type:String,
        required:false
    },
    updated_at:{
        type:String,
        required:false
    },
    otp:{
        type:Number,
        required:false
    }
});
module.exports=mongoose.model("users",UserSchema);