const mongoose=require("mongoose")
const AdminSchema=mongoose.Schema({
    adminId:mongoose.Schema.Types.ObjectId,
    fullName:{
        type:String,
        required:true,
        unique: false
    },
    email:{
        type:String,
        required:true,
        unique: true
    },
    contact:{
        type:Number,
        required:true,
        unique: true
    },
    password:{
        type:String,
        required:true
    },
    isAdmin:{
        type:Boolean,
        required:false
    }, 
});
module.exports=mongoose.model("admin",AdminSchema);